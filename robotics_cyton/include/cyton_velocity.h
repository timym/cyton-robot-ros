#ifndef CYTON_VELOCITY
#define CYTON_VELOCITY

#include <ros/ros.h>
#include <atomic>
#include <dynamixel_controllers/TorqueEnable.h>
#include "robotics_cyton/TorqueEnable.h"
#include "robotics_cyton/Float64.h"
#include "robotics_cyton/cyton_move_all_joints.h"
#include "robotics_cyton/cyton_move_single_joint.h"
#include <thread>
#include <std_msgs/Byte.h>
#include <std_msgs/Float64MultiArray.h>
#include <std_msgs/Float64.h>

#include <eigen3/Eigen/Dense>
#include <sensor_msgs/JointState.h>

#include <mutex>

#include <std_srvs/Empty.h>

class CytonVelocity
{
public:
    CytonVelocity(ros::NodeHandle &nh);
    bool setSpeed();

    bool stop();
    bool run();
    bool isRunning();
private:

    CytonVelocity();
    ros::NodeHandle _nh;

    std::atomic<bool> _isRunning, _inUse;

    void thread();
    std::thread* velocityThread;
    //callbacks
//    void timerCallback(const ros::TimerEvent&);


    void jointStatesCallback(const sensor_msgs::JointStateConstPtr &msg);
    void positionControlCb(const std_msgs::Float64MultiArrayConstPtr &msg);
    void clawPositionCallback(const std_msgs::Float64ConstPtr &msg);
    bool setFrequency();



    int _requiredFrequency;
    ros::Subscriber velocityRequested, jointStatesSub, positionControlSub, clawPositionSub;

    void getVelocityCb(const std_msgs::Float64MultiArrayConstPtr &msg);

    std::vector<double> velocityValues;

    ros::Time currentTime, lastTime;

    ros::ServiceServer enableService, setSpeedService, goHomeService, positionControlServices, singleJointService, clawPositionService;

    //services
    bool enableRobot(dynamixel_controllers::TorqueEnable::Request& req, dynamixel_controllers::TorqueEnable::Response& res);
    bool goHome(std_srvs::Empty::Request &req, std_srvs::Empty::Response &res);
    bool positionControlService(robotics_cyton::cyton_move_all_joints::Request &req, robotics_cyton::cyton_move_all_joints::Response &res);
    bool singlePositionControlService(robotics_cyton::cyton_move_single_joint::Request &req, robotics_cyton::cyton_move_single_joint::Response &res);
    bool setClawPosition(robotics_cyton::Float64::Request& req, robotics_cyton::Float64::Response& res);

//    bool goToPosition(robotics_cyton::)


    std::mutex _jointStatesMutex;
    Eigen::VectorXd _currentJointStates;

    Eigen::VectorXd _maxJointLimits, _minJointLimits;

    double _maxVelocity;

    std::mutex _velocityMutex;
    Eigen::VectorXd commandedVelocity;

    ros::AsyncSpinner* asyncSpinner;

    void getJointLimits();
    bool checkJointLimits(int i);

    std::atomic<bool> _joint_states_received;

    std::mutex _clawPositionMutex;
    double clawPositionCommand;

    double _jointLimitThreshold;

    double calculateClawCommand(double millimeter);

};

#endif