#include "cyton_velocity.h"

CytonVelocity::CytonVelocity(ros::NodeHandle &nh):
    _nh(nh)
{
    _isRunning = false;
    _inUse = false;
    _joint_states_received = false;

    _currentJointStates.resize(7,1);
    _currentJointStates = Eigen::VectorXd::Zero(7,1);

    commandedVelocity.resize(7,1);
    commandedVelocity = Eigen::VectorXd::Zero(7,1);

    _maxJointLimits.resize(7,1);
    _maxJointLimits = Eigen::VectorXd::Zero(7,1);

    _minJointLimits.resize(7,1);
    _minJointLimits = Eigen::VectorXd::Zero(7,1);

    _maxVelocity = 1.0;//read from parameter server
    _requiredFrequency = 10;//read from parameter server

    getJointLimits();

    _jointLimitThreshold = 0.05;

}

void CytonVelocity::getJointLimits(){
    double radians_per_encoder_tick, init;
    for(int i = 0; i < 7; ++i){
        _nh.getParam("/dynamixel/pan_tilt_port/" + std::to_string(i) + "/radians_per_encoder_tick",radians_per_encoder_tick);
        _nh.getParam("/joint" + std::to_string(i+1) + "_controller/motor/init",init);
        _nh.getParam("/joint" + std::to_string(i+1) + "_controller/motor/min",_minJointLimits[i]);
        _minJointLimits[i] = (_minJointLimits[i] - init) * radians_per_encoder_tick;
        _nh.getParam("/joint" + std::to_string(i+1) + "_controller/motor/max",_maxJointLimits[i]);
        _maxJointLimits[i] = (_maxJointLimits[i] - init) * radians_per_encoder_tick;
    }
}

void CytonVelocity::jointStatesCallback(const sensor_msgs::JointStateConstPtr &msg) {
    std::lock_guard<std::mutex> lk (_jointStatesMutex);
    _currentJointStates << msg->position[0], msg->position[1], msg->position[2], msg->position[3], msg->position[4], msg->position[5], msg->position[6];

    _joint_states_received = true;
}

void CytonVelocity::clawPositionCallback(const std_msgs::Float64ConstPtr &msg){
    std::lock_guard<std::mutex> lk (_clawPositionMutex);
    clawPositionCommand = msg->data;
}

bool CytonVelocity::setClawPosition(robotics_cyton::Float64::Request& req, robotics_cyton::Float64::Response& res){
//    double value = (_maxJointLimits[7]-_minJointLimits[7])* req.data

    std::lock_guard<std::mutex> lk (_clawPositionMutex);
    clawPositionCommand = calculateClawCommand(req.data);
//    std::cout << clawPositionCommand << std::endl;

    return true;
}

bool CytonVelocity::run(){
    ROS_INFO("Running");

    if(!_isRunning){
        enableService = _nh.advertiseService("enableCyton", &CytonVelocity::enableRobot, this);
        goHomeService = _nh.advertiseService("goHome", &CytonVelocity::goHome, this);
        positionControlServices = _nh.advertiseService("GoToPosition", &CytonVelocity::positionControlService,this);
        singleJointService = _nh.advertiseService("MoveSingleJoint", &CytonVelocity::singlePositionControlService,this);
        clawPositionService = _nh.advertiseService("ClawPosition", &CytonVelocity::setClawPosition,this);

        velocityRequested = _nh.subscribe("/cyton_velocity_commands",1,&CytonVelocity::getVelocityCb, this);
        jointStatesSub = _nh.subscribe("/joint_states",1, &CytonVelocity::jointStatesCallback, this);
        positionControlSub = _nh.subscribe("/cyton_position_commands",1, &CytonVelocity::positionControlCb, this);
        clawPositionSub = _nh.subscribe("/cyton_claw_position",1, &CytonVelocity::clawPositionCallback, this);

        velocityThread = new std::thread(&CytonVelocity::thread, this);

        lastTime = ros::Time::now();
        currentTime = ros::Time::now();
        asyncSpinner = new ros::AsyncSpinner(4);
        asyncSpinner->start();
        _isRunning = true;
        return true;
    }

    ROS_ERROR("Already Running");
    return false;

}

bool CytonVelocity::stop(){
    if(!_isRunning){
        ROS_WARN("Not currently running");
        return false;
    }
    if(velocityThread->joinable()) {
        velocityThread->join();
        enableService.shutdown();
        velocityRequested.shutdown();
        positionControlServices.shutdown();
        singleJointService.shutdown();
        asyncSpinner->stop();
        _isRunning = false;
        return true;
    }
    ROS_DEBUG("Thread Not stopping");
    return false;
}

void CytonVelocity::getVelocityCb(const std_msgs::Float64MultiArrayConstPtr &msg) {
    lastTime = ros::Time::now();

    if(msg->data.size() != 7){
        ROS_ERROR("INCORRECT NUMBER OF VALUES PUBLISHED");
    }
    else{
        std::lock_guard<std::mutex> lk(_velocityMutex);
        for(unsigned int i = 0; i < 7; ++i){
            commandedVelocity[i] = msg->data.at(i); //publish request values from subscriber
        }
    }
}


bool CytonVelocity::enableRobot(dynamixel_controllers::TorqueEnable::Request& req, dynamixel_controllers::TorqueEnable::Response& res){//use the dynamixel torque enable and if true set to 3 false = 0

    robotics_cyton::TorqueEnable torque_enable;


    std_msgs::Byte input;

    if(req.torque_enable == true){
        input.data = 3;
    }
    else{
        input.data = false;
    }
//    input.data = req.torque_enable;



    ROS_INFO("Attempting to enable/disable robot");
//    std::cout << "Torque Enable Input: " << torque_enable << std::endl;

    if(!(input.data == 0 || input.data == 3)){
        ROS_WARN("Invalid Torque Enabling Value In Velocity Mode");
        return false;
    }

    torque_enable.request.torque_enable = input.data;

    for(int i = 0; i < 7; ++i){
        std::string enable_joint_name="joint";
        enable_joint_name.append(std::to_string(i+1));
        enable_joint_name.append("_controller/torque_enable");
        ros::ServiceClient client = _nh.serviceClient<robotics_cyton::TorqueEnable>(enable_joint_name);
        client.call(torque_enable);
//        ros::service::call(enable_joint_name,torque_enable);
    }
    ROS_INFO("Robot successfully enabled/disabled");
    return true;
}

bool CytonVelocity::goHome(std_srvs::Empty::Request &req, std_srvs::Empty::Response &res) {

    if(_inUse){
        ROS_ERROR("Unable to complete this action 'goHome' due to other services already running");
        return false;
    }
    _inUse = true;
    ROS_INFO("Moving to home position, this may take up to 10 seconds");

    double kp = 1.0, ki = 0.1;

    bool home = false;
    Eigen::VectorXd desired_position, error, corrected_velocity;
    desired_position.resize(7,1);
    error.resize(7,1);
    corrected_velocity.resize(7,1);
    desired_position = Eigen::VectorXd::Zero(7,1);
    error = Eigen::VectorXd::Zero(7,1);
    corrected_velocity = Eigen::VectorXd::Zero(7,1);

//    desired_position << 0, 0, 0, 0, 0, 0, 0;
    std::unique_lock<std::mutex> lk (_jointStatesMutex);
    error = desired_position - _currentJointStates;
    lk.unlock();

    std::unique_lock<std::mutex> lk2(_velocityMutex);
    lk2.unlock();
//    std_msgs::Float64 msg;
    ros::Rate rate(50);

    while(ros::ok() && !home){
        lastTime = ros::Time::now();
        lk.lock();
        error = desired_position - _currentJointStates;
        lk.unlock();
        corrected_velocity = kp * error;
        lk2.lock();
        commandedVelocity = corrected_velocity;
        lk2.unlock();
        rate.sleep();
        if(corrected_velocity.norm() < 0.01){
            home = true;
        }
    }

    _inUse = false;
    ROS_INFO("Robot Homed");
    return true;
}

bool CytonVelocity::positionControlService(robotics_cyton::cyton_move_all_joints::Request &req, robotics_cyton::cyton_move_all_joints::Response &res) {

    if(_inUse){
        ROS_ERROR("Unable to complete this action 'positionControl' due to other services already running");
        return false;
    }
    _inUse = true;
    ROS_INFO("Moving to position xx, this will return true once the robots reaches its location");

    double kp = 1.0, ki = 0.1;

    bool atLocation = false;
    Eigen::VectorXd desired_position, error, corrected_velocity;
    desired_position.resize(7,1);
    error.resize(7,1);
    corrected_velocity.resize(7,1);
    desired_position = Eigen::VectorXd::Zero(7,1);
    error = Eigen::VectorXd::Zero(7,1);
    corrected_velocity = Eigen::VectorXd::Zero(7,1);

    for(unsigned int i = 0; i < 7; ++i){
        desired_position[i] = req.jointStates.at(i);
    }
//    desired_position << 0, 0, 0, 0, 0, 0, 0;
    std::unique_lock<std::mutex> lk (_jointStatesMutex);
    error = desired_position - _currentJointStates;
    lk.unlock();

    std::unique_lock<std::mutex> lk2(_velocityMutex);
    lk2.unlock();
//    std_msgs::Float64 msg;
    ros::Rate rate(50);

    while(ros::ok() && !atLocation){
        lastTime = ros::Time::now();
        lk.lock();
        error = desired_position - _currentJointStates;
        lk.unlock();
        corrected_velocity = kp * error;
        lk2.lock();
        commandedVelocity = corrected_velocity;
        lk2.unlock();
        rate.sleep();
        if(corrected_velocity.norm() < 0.01){
            atLocation = true;
        }
    }

    _inUse = false;

    return true;
}

bool CytonVelocity::singlePositionControlService(robotics_cyton::cyton_move_single_joint::Request &req, robotics_cyton::cyton_move_single_joint::Response &res) {

    if(_inUse){
        ROS_ERROR("Unable to complete this action 'positionControl' due to other services already running");
        return false;
    }
    _inUse = true;
    ROS_INFO("Moving to position xx, this will return true once the robots reaches its location");

    if(req.joint_number >= 7 || req.joint_number < 0){
        ROS_ERROR("INCORRECT Joint Number");
        return false;
    }

    double kp = 1.0, ki = 0.1;

    bool atLocation = false;
    Eigen::VectorXd desired_position, error, corrected_velocity;
    desired_position.resize(7,1);
    error.resize(7,1);
    corrected_velocity.resize(7,1);
    desired_position = Eigen::VectorXd::Zero(7,1);
    error = Eigen::VectorXd::Zero(7,1);
    corrected_velocity = Eigen::VectorXd::Zero(7,1);




//    desired_position << 0, 0, 0, 0, 0, 0, 0;
    std::unique_lock<std::mutex> lk (_jointStatesMutex);
    for(unsigned int i = 0; i < 7; ++i){
        desired_position = _currentJointStates;
    }
    desired_position[req.joint_number] = req.position;
    error = desired_position - _currentJointStates;
    lk.unlock();

    std::unique_lock<std::mutex> lk2(_velocityMutex);
    lk2.unlock();
//    std_msgs::Float64 msg;
    ros::Rate rate(50);

    while(ros::ok() && !atLocation){
        lastTime = ros::Time::now();
        lk.lock();
        error = desired_position - _currentJointStates;
        lk.unlock();
        corrected_velocity = kp * error;
        lk2.lock();
        commandedVelocity = corrected_velocity;
        lk2.unlock();
        rate.sleep();
        if(corrected_velocity.norm() < 0.01){
            atLocation = true;
        }
    }

    _inUse = false;

    return true;
}

void CytonVelocity::positionControlCb(const std_msgs::Float64MultiArrayConstPtr &msg) {

    if(_inUse){
        ROS_ERROR("Unable to complete this action 'PositionControl' due to other services already running");
        return;
    }
    else{
        _inUse = true;
        double kp = 1.0, ki = 0.1;

//        bool home = false;
        Eigen::VectorXd desired_position, error, corrected_velocity;
        desired_position.resize(7,1);
        error.resize(7,1);
        corrected_velocity.resize(7,1);
        desired_position = Eigen::VectorXd::Zero(7,1);
        error = Eigen::VectorXd::Zero(7,1);
        corrected_velocity = Eigen::VectorXd::Zero(7,1);

        if(msg->data.size() != 7){
            ROS_ERROR("INCORRECT NUMBER OF VALUES PUBLISHED");
        }
        else{
            for(unsigned int i = 0; i < 7; ++i){
                desired_position[i] = msg->data.at(i);
            }

            std::unique_lock<std::mutex> lk (_jointStatesMutex);
            error = desired_position - _currentJointStates;
            lk.unlock();

            lastTime = ros::Time::now();
            lk.lock();
            error = desired_position - _currentJointStates;
            lk.unlock();
            corrected_velocity = kp * error;
            std::unique_lock<std::mutex> lk2(_velocityMutex);
            commandedVelocity = corrected_velocity;
            lk2.unlock();
        }
        _inUse = false;
    }
}

void CytonVelocity::thread() {
    ROS_INFO("Ready to receive commands. Note: velocity & position publishers must be publish commands to %s at a rate of at least %dhz", velocityRequested.getTopic().c_str(), _requiredFrequency);// change hz to required value (if((currentTime - lastTime) > ros::Duration(0.1)))
    std::vector<ros::Publisher> joint_controller_publishers;
    for(int i = 0; i < 7; ++i){
        std::string joint_controller_name="joint";
        joint_controller_name.append(std::to_string(i+1));
        joint_controller_name.append("_controller/command");
        joint_controller_publishers.push_back(_nh.advertise<std_msgs::Float64>(joint_controller_name,1));
    }
    joint_controller_publishers.push_back(_nh.advertise<std_msgs::Float64>("claw_controller/position",1));

    std_msgs::Float64 msg;


//    lk.unlock();

    ros::Rate rate(50);//get value from param
    while(ros::ok() && !_joint_states_received){
        rate.sleep();
    }
    std::unique_lock<std::mutex> lk(_velocityMutex);
    std::unique_lock<std::mutex> lk2(_clawPositionMutex);
    clawPositionCommand = calculateClawCommand(26.5);//open position
//    std::cout << clawPositionCommand << std::endl;
//    std::cout << "2: " << _currentJointStates[7] << std::endl;
    lk.unlock();
    lk2.unlock();




    while(ros::ok() && _isRunning){
        if(_joint_states_received) {
            currentTime = ros::Time::now();
            if ((currentTime - lastTime) > ros::Duration(1.0 / _requiredFrequency)) {
                for (int i = 0; i < 7; ++i) {
                    msg.data = 0.0;
                    joint_controller_publishers[i].publish(msg); //publish request values from subscriber
                }
            } else {
                lk.lock();
                for (int i = 0; i < 7; ++i) {
                    if (commandedVelocity[i] > _maxVelocity) {
                        commandedVelocity[i] = _maxVelocity;
                    } else if (commandedVelocity[i] < -_maxVelocity) {
                        commandedVelocity[i] = -_maxVelocity;
                    }
                    if (checkJointLimits(i)) {
                        msg.data = (float) commandedVelocity[i];
                    } else {
                        msg.data = 0.0;
                        std::lock_guard<std::mutex> lk(_jointStatesMutex);
                        ROS_ERROR("STOPPING MOTION: Motor Passed Joint Limit, Motor Number %d, JointState: %f, Joint Limits: [%f - %f]", i, _currentJointStates[i], _minJointLimits[i], _maxJointLimits[i]);
                    }
                    joint_controller_publishers[i].publish(msg); //publish request values from subscriber
                }
                lk.unlock();
            }
            lk2.lock();
            msg.data = (float) clawPositionCommand;
            lk2.unlock();
//                ROS_INFO("HERE");
//            std::cout << msg.data << std::endl;
            joint_controller_publishers[7].publish(msg); //publish request values from subscriber
        }
        rate.sleep();
    }

}


bool CytonVelocity::setFrequency() {//needs service
    _requiredFrequency = 10;
    ROS_INFO("Required Frequency Changed to %dhz", _requiredFrequency);
}

bool CytonVelocity::isRunning() {
    return _isRunning;
}

bool CytonVelocity::checkJointLimits(int i){
    std::lock_guard<std::mutex> lk(_jointStatesMutex);
    return !(_currentJointStates[i] < _minJointLimits[i] || _currentJointStates[i] > _maxJointLimits[i]);
}

double CytonVelocity::calculateClawCommand(double millimeter){
    return ((1.4/26.5)* millimeter - 1.4);

}






