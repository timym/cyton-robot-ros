#include <ros/ros.h>
#include <trajectory_msgs/JointTrajectory.h>
//#include <trajectory_msgs/JointTrajectoryPoint.h>
#include <string>
#include <std_msgs/Float64MultiArray.h>

int main(int argc, char** argv)
{
    ros::init(argc, argv, "move_group_tutorial");
    ros::NodeHandle node_handle("~");
    ros::NodeHandle nh;
    ros::Publisher vel_pub = nh.advertise<trajectory_msgs::JointTrajectory>("cute_arm_controller/command",1);
ros::Publisher floatpub = nh.advertise<std_msgs::Float64MultiArray>("cyton_position_commands",1);


    std_msgs::Float64MultiArray msg;

    msg.data.push_back(-1.5);
    msg.data.push_back(0);
    msg.data.push_back(0);
    msg.data.push_back(0);
    msg.data.push_back(0);
    msg.data.push_back(0);
    msg.data.push_back(0);

//    msg.layout.dim


//
////    ros::AsyncSpinner spinner(0);
//
//    trajectory_msgs::JointTrajectory msg;
//
//    trajectory_msgs::JointTrajectoryPoint point;
////    point.
//
//
//    for(int i = 0; i < 7; ++i){
//        std::string joint_name="joint";
//        std::string joint_num= std::to_string(i+1);
//        msg.joint_names.push_back(joint_name+joint_num);
//        point.positions.push_back(0.2);
//        point.velocities.push_back(0.1);
//
//    }
//
//    std::cout << msg.joint_names.size() << std::endl;
//    std::cout << point.velocities.size() << std::endl;
//
//    point.time_from_start = ros::Duration(0.5);
//
//
//    msg.points.push_back(point);
    ros::Rate rate(10);
    while(ros::ok()){
        floatpub.publish(msg);
//        vel_pub.publish(msg);
        rate.sleep();
    }


//    spinner.start();
//    ros::waitForShutdown();


    return 0;
}
