Modified Copy of https://github.com/hans-robot/cute_robot for robotics UTS 2018


Modified components:
 - Driver - additional capacity for velocity mode
          - added functions


 - Torque Controller - Enable torque (mode 3)

 - Cute Bringup Changed





Removed:

 - MoveIt Components

 - Teleop Components

 - Roscontrol




Notes:

 - Components are still included here that are not used
 - Tested on raspberry pi 
 - Included matlab gen file - updated wiki
